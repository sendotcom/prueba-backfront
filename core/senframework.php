<?php
//Incluimos la clase sfModel que es la de Base de datos
//include("model/fModel.php");
#include("sfModelPublic.php");
#include("library/misc.php");


class senframework {
    //Variable de clase para el modelo
    var $modelInclude;
    var $modelPublic;

    function __construct(){
        //Instanciamos el modelo dentro del constructor y lo asignamos a la variable de clase
       // $this->modelInclude = new sfModel();
       // $this->modelPublic = new sfModelPublic();
    }

    //Renderizado principal
    function viewInclude($vista='',$variables = array()){

        extract ($variables);
        ob_start();
        include($vista);
        $res = ob_get_contents();

        return $res;
        ob_end_clean();
    }

}
